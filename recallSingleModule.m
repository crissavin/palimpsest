% Decritpion: single module recall dynamics for Fusi's carcade model
% uses a factorized approximation of the posterior P(x|W,cue), after marginalizing t
% see Savin et al, PLoS CB 2014 for details
% Author: C.Savin
clear,clc
rng(1234, 'twister')
dynamicsType = 'single module, factorized W distribution';
params = getDefaultParams(); % different variants by extra parameter
N = params.N;UseT = params.UseT;
Tsamples = params.Tsamples;

% local details for experiment: which ages how many trials for each
TM = 1; % no trials per pattern age
trange = [1 2 5 10 25 50 100]; %pattern ages tested
%% statistics of storage
P_wv = getPwv(params.lrule.n); % map v->w
lrule_details = getMC(getMx(params.f,params.lrule),params.f); %transition operator
if params.useExactCascade
    p_inf = getPinfCascade(params.f,params.lrule); %use analytics when possible
end

%params for strength of cue evidence
[cst1,cst2]=getCst12(params.f,params.r);

% strength of recurrents, given prior over t
[fc1, fc2] = getFs(getPw(P_wv,getPvPriorT(lrule_details,params)));
[a1pre, a2pre, a3pre, a4pre] = getAs(fc1);
[a1post, a2post, a3post, a4post] = getAs(fc2);
%% recall dynamics

for i = 1:length(trange)
    ttrue = trange(i);
   
    Pw = getPw(P_wv,getPv(lrule_details,ttrue,params.lrule.approx)); % P(w|t_true,x) marginals
    
    for tr = 1:TM
        mask = rand(N)<=params.psyn; % sparse connectivity
        mask(eye(N)==1) = 0; % no self connections
        
        % store one pattern with age ttrue
        stored = randPattern(params.N,params.f);
        W = storePatternIndep(stored, Pw);
        
        %recall pattern
        recall_patt = addNoise(stored,params.r);
        
        x = recall_patt;
        all_xm=0;
        
        tic
        for t=1:Tsamples
            idx = randi(N); % asynch. update, in random order
            rest = ((1:N)~=idx);
            synapsesij = rest & mask(idx,:); %list of presynaptic partners
            synapsesji = rest & mask(:,idx)'; %       postsynaptic
            
            evidenceW = a1pre*W(idx,synapsesij)*x(synapsesij)+ sum(a2pre*W(idx,synapsesij)+a3pre*x(synapsesij)'+a4pre)...
                + params.noApprox*(a1post*W(synapsesji,idx)'*x(synapsesji)+sum(a2post*W(synapsesji,idx)+a3post*x(synapsesji)+a4post));%no approx
            
            lpr = cst1 + cst2*recall_patt(idx)+ evidenceW;
            
            x(idx) = rand < (1/(1+exp(-lpr))); %Gibbs sampling stochastic dynamics
            
            if t>UseT %skip initial burn-in period
                all_xm = all_xm+ x;  
            end
        end
        toc
        
        mx = all_xm/(Tsamples-UseT); %log posterior mean
        err(tr,i) = mean((stored-mx).^2); % m.s.error
        h(tr,i)= avghpy(mx); %average neuron response entropy (as proxy for uncertainty)
    end
end