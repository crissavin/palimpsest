function [fc1,fc2] = getFs(P_w)

m = size(P_w,1);
ValInf = 1E25;

fc1 = reshape(log(P_w(:,2,:)) - log(P_w(:,1,:)),m,2);
fc2 = reshape(log(P_w(:,:,2)) - log(P_w(:,:,1)),m,2);

idx = isinf(fc1);
fc1(idx) = sign(fc1(idx)).*ValInf;

idx = isinf(fc2);
fc2(idx) = sign(fc2(idx)).*ValInf;
