function x = randPattern(N,f)
%random pattern, factorized bernoulli

x = rand(N,1) < f;