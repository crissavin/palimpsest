function mh = avghpy(mx)

tmp = -mx.*log2(mx) - (1-mx).*log2(1-mx);
tmp(isnan(tmp))=0;
mh = mean(tmp);