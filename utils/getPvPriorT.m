function [P_v] = getPvPriorT(lrule_details,params)
% compute P(W_t|xi,xj) for all activity pairs, prior for t

[Mx,p_inf, X, li] = deal(lrule_details.Mx,lrule_details.p_inf,...
    lrule_details.X,lrule_details.li);
nn = length(li);
Dt = zeros(nn);
k = params.k;

switch params.prior
    case 'geom'
        Dt(eye(nn)==1) = (1-k)./(1-k*li);
    case 'powl'
        alfa = params.alfa;
        Dt(eye(nn)==1) = polylog(alfa,k*li)./(li*polylog(alfa,k));
    otherwise
        error('unknown t prior')
end


Minf = X*Dt/X;
P_v = zeros(nn,2,2);
for i=1:2
    for j=1:2
        P_v(:,i,j) = Minf*Mx{i,j}*p_inf;
    end
end
P_v = real(P_v);
