function params = getDefaultParams(experimentType)
% 'default' - fully connected,
if nargin < 1,  experimentType = 'default', end
params.experiment  = experimentType;

%age distribution
params.prior = 'geom';
params.k = 0.95; % mean 20
params.alfa = 0.5;

%patterns
params.N = 1000;
params.psyn = 1; %synaptic density
params.f = 0.5 ;  % pattern sparsity
params.r = 0.1;  % noise level; symmetric binary channel

%plasticity
lrule.syntype = 'cascade'; %fusi/kempter
lrule.learnRule = 'postGated';
params.useExactCascade =1; % use analytic solutions whenever possible
lrule.n = 5;
lrule.qpot = 1;
lrule.qdep = 1;
lrule.Unif = 1; % cascade correction for end state?
lrule.approx = 0; % inference: use eigsystem for computing M^t?
params.lrule = lrule;

lruleF = lrule;
lruleF.qpot = 0.75;
lruleF.qdep = 0.75;
params.lruleF = lruleF;

%recall
params.noApprox= 1; % recall: use all synapses or just incoming?
params.tMax = 150; %horizon for pattern ages in fam module (truncated tail)


switch experimentType
    case 'default'
    case 'power law prior for age'
        params.k = 0.97; %matched to learning rule
        params.alfa = 0.5; params.prior = 'powl';
    case 'cascade new'
        params.k = 0.97; %matched to learning rule
        params.alfa = 0.5; params.prior = 'powl';
        params.lruleF.syntype = 'cascadeNew'
        params.lruleF.qpot = 0.5;
        params.lruleF.qdep = 0.5;
    case 'sparse patterns'
        params.f = 0.1;
    case 'W energy landscape'
        params.r = 0.5;
    otherwise error('unknown experiment type')
end

params.Tsamples = 100*params.N;
params.UseT = 10*params.N;