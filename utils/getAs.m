function [a1, a2, a3, a4] = getAs(fc)

a1 = fc(2,2)-fc(1,2)-fc(2,1)+fc(1,1); %correct for possible numerical issues?
a2 = fc(2,1)-fc(1,1); % w weight
a3 = fc(1,2)-fc(1,1); % x weight
a4 = fc(1,1);