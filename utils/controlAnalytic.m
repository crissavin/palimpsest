function err = controlAnalytic(f,r)

p00 = (1-f)*(1-r)*(f*r/(f*r+(1-f)*(1-r)))^2;
p01 = r*(1-f)*(f*(1-r)/(f*(1-r)+(1-f)*r))^2;
p10 = f*r*(f*r/(f*r+(1-f)*(1-r)) - 1)^2;
p11 = f*(1-r)*(f*(1-r)/(f*(1-r)+(1-f)*r) -1)^2;
err = p00+p01+p10+p11;