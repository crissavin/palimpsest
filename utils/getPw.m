function P_w = getPw(P_wv,P_v)
% project cond distrib P(v|xi,xj)
% from state space to weight space
nn = size(P_wv,1);
P_w = zeros(nn,2,2);

for i = 1:2 %post
    for j=1:2 %pre
        P_w(:,i,j) = P_wv*P_v(:,i,j);
    end
end