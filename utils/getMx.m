function Mx = getMx(f, lrule_details)
%constructs conditional operators for all possible activity patterns
% allowed synapse types : serial or cascade

[n,qpot,qdep,Unif,syntype,learnRule] = deal(lrule_details.n,lrule_details.qpot,...
    lrule_details.qdep,lrule_details.Unif,lrule_details.syntype,lrule_details.learnRule);

switch syntype
    case 'cascade'
        [Mp,Mm] = getMCascade(f,qpot,qdep,n,Unif&(n>1),1);
    case 'cascadeNew'
        [Mp,Mm] = getMCascadeNew(f,qpot,qdep,n,Unif&(n>1),1);
    case 'serial'
        [Mp,Mm] = getMSerial(f,qpot,qdep,n);
    otherwise
        error('wrong syn type')
end


switch learnRule
    case 'postGated'
        Mx{1,1} = eye(2*n); %xi,xj - post, pre
        Mx{1,2} = eye(2*n);
        Mx{2,1} = Mm;
        Mx{2,2} = Mp;
    case 'preGated'
        Mx{1,1} = eye(2*n); %xi,xj - post, pre
        Mx{2,1} = eye(2*n);
        Mx{1,2} = Mm;
        Mx{2,2} = Mp;
    case 'cov'
        Mx{1,1} = Mp; %xi,xj - post, pre
        Mx{1,2} = Mm;
        Mx{2,1} = Mm;
        Mx{2,2} = Mp;
    otherwise
        error('unknown learning rule')
end