function p_inf = getPinfCascade(f,params)
%analytical computation of p_inf
[qpot,qdep,n] = deal(params.qpot,params.qdep,params.n);

p_inf = [(qdep/qpot)*((1-f)/f)*ones(n,1); ones(n,1)];
p_inf = p_inf./sum(p_inf);