function ptn = getPriorT(params)
[k, alfa, tMax] = deal(params.k,params.alfa,params.tMax);

switch params.prior
    case 'geom'
        ptn = (1-k) *(k.^((1:tMax)-1));
        ptn(tMax) = k^(tMax-1);
    case 'powl'
        tt = (1:tMax);
        ptn = k.^tt .* tt.^-alfa;
    otherwise
        error('prior not implemented')
end

%todo other priors
    

ptn = ptn'./sum(ptn);