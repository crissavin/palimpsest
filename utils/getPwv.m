function P_wv = getPwv(n)
%map to binary observables

P_wv = zeros(2,2*n);
P_wv(1,1:n) = 1;
P_wv(2,n+(1:n)) = 1;

