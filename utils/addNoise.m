function y = addNoise(x,r)

y = xor(x,rand(size(x))<r);