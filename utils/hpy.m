function hh = hpy(p)

assert(sum(p)==1);
hh = -sum(p(idx).*log2(p(idx)));
