function [Mp,Mm] = getMCascade(f,qpot,qdep,n,Unif,xmax)
% transition matrix for Fusi 2005 cascade

% correction for sparse patterns
crp = qdep*(1-f)/f;
crm = qpot*f/(1-f);

% take largest allowed x
x = min([xmax 1/(1+crp) 1/(1+crm)]);

qp = qpot * x.^((1:n)-1);
if Unif && n>1
    qp(n) = qpot * x^(n-1)/(1-x); %makes stationary distribution uniform
end

qm = qdep * x.^((1:n)-1);
if Unif && n>1
    qm(n) = qdep * x^(n-1)/(1-x); %makes stationary distribution uniform
end

pp = min(1, crp*(x.^(1:n))/(1-x));
pm = min(1, crm*(x.^(1:n))/(1-x));

Mp = zeros(2*n); % first n states correspond to weak syn
Mp(sub2ind(size(Mp),1:n,1:n)) = 1-qp;
Mp(n+1,1:n) = qp;
Mp(sub2ind(size(Mp),n+(1:n),n+(1:n))) = 1-pp;
Mp(sub2ind(size(Mp),n+(2:n),n-1+(2:n))) = pp(1:(n-1));
Mp(2*n,2*n) = 1;

Mm = zeros(2*n);
Mm(sub2ind(size(Mm),1:n,1:n)) = 1-pm;
Mm(sub2ind(size(Mm),2:n,1:(n-1))) = pm(1:(n-1));
Mm(n,n) = 1;
Mm(sub2ind(size(Mm),n+(1:n),n+(1:n))) = 1-qm;
Mm(1,n+(1:n)) = qm;