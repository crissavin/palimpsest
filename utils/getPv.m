function P_v = getPv(lrule_details,t,approx)
%compute P(v_ij|x_i,x_j,t) for all activity pairs

[Mx,M, p_inf, X, li] = deal(lrule_details.Mx,lrule_details.M,...
                    lrule_details.p_inf,lrule_details.X,lrule_details.li);
nn = size(M,1);
if approx 
    Dt = zeros(nn);
    Dt(eye(nn)==1) = li.^(t-1);
    Mrest = X*Dt/X;
else
    Mrest = M^(t-1);
end

P_v = zeros(nn,2,2);  % post, pre
for i=1:2
    for j = 1:2
        P_v(:,i,j) = Mrest*Mx{i,j}*p_inf;
    end
end
