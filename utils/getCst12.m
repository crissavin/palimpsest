function [cst1,cst2]=getCst12(f,r)
cst1 = log(f/(1-f))+log(r/(1-r));
cst2 = 2*log((1-r)/r);
