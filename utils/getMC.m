function lrd = getMC(Mx,f)
% given the set M{xi,xj} estimate Mbar, eigenvals and eigenvectors,
% stationary distribution
nn = size(Mx{1,1},1);

M = ((1-f)^2)*Mx{1,1} + f*(1-f)*Mx{1,2} + f*(1-f)*Mx{2,1} + (f^2)*Mx{2,2};
[X,D] = eig(M);

% li = real(D(eye(nn)==1));
li = D(eye(nn)==1);
[~,idx] = max(li);

% X = real(X); %to avoid numerical issues
assert(1-li(idx)<1e-12)
p_inf = abs(X(:,idx));

lrd.p_inf = p_inf./sum(p_inf);
lrd.X = X;
lrd.li = li;
lrd.M = M;
lrd.Mx = Mx;

