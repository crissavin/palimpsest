function Vn = storePatternOne(V,x, lrule_details,oldF,debugMode)
% one step of Markov transition for recurrent network, sped up
if nargin < 4, oldF=0;end
if nargin < 5, debugMode=0;end

Mx = lrule_details.Mx;
N=size(V,1);
n = size(Mx{1,1},1);
Vn = V;

if debugMode, tic, end
if oldF
    for i=1:N
        for j=1:N
            Vn(i,j) = sampleD(Mx{x(i)+1,x(j)+1}(:,V(i,j)),1); %post,pre
        end
    end
else
    %figure out how many synapses need to undergo the same type of
    %transiton and sample them in one go
    xx = reshape(x+1,N,1);
    Xpost = repmat(xx,1,N);
    Xpre = repmat(xx',N,1);
    for xpost = [1,2]
        for xpre=[1,2]
            if ~all(all(Mx{xpost,xpre}==eye(n))) %don't bother if same as before
                tmpX = and(Xpre==xpre,Xpost==xpost);
                for v = 1:n
                    mask = and(V==v, tmpX);
                    Vn(mask) = sampleD(Mx{xpost,xpre}(:,v), sum(mask(:)));
                end
            end
        end
    end
end
if debugMode, toc, end