function [apre, apost] = getAsAllt(logPwt)

Pwt = exp(logPwt);
tMax  = size(Pwt,1);
apre  = zeros(tMax,4);
apost = zeros(tMax,4);

for t = 1:tMax
    [fc1, fc2] = getFs(squeeze(Pwt(t,:,:,:)));
    [a1pre, a2pre, a3pre, a4pre] = getAs(fc1);
    [a1post, a2post, a3post, a4post] = getAs(fc2);
    apre(t,:) = [a1pre, a2pre, a3pre, a4pre];
    apost(t,:) = [a1post, a2post, a3post, a4post];
end