function W = storePatternIndep(x, Pw)
%store pattern x, according to distribution specified by pw
%assumes factorized distribution, no correlations!
% ignores effect of

N = numel(x);
W = zeros(N);
%indices: post, pre 
for i=1:N    
    W(i,:) = rand(N,1) < squeeze(Pw(2,x(i)+1,x+1));
end
