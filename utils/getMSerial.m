function [Mp,Mm] = getMSerial(f,qpot,qdep,n)
% transition matrices for serial model
% serial cascade from Leibold Kempter 2008

if qdep <0 %use optimal
    qdep = qpot*f/(1-f);
end

Mp = zeros(2*n); % first n states correspond to weak syn; indexing different from cascade model!
Mp(2:(2*n),1:(2*n-1)) = qpot*eye(2*n-1);
Mp(eye(2*n)==1) = 1-qpot;
Mp(2*n,2*n)=1;

Mm = zeros(2*n);
Mm(1:(2*n-1),2:(2*n)) = qdep*eye(2*n-1);
Mm(eye(2*n)==1) = 1-qdep;
Mm(1,1)=1;